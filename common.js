class TrafficLighter {
    constructor(targetElement) {
      this.targetElement = targetElement;
      this.colors = ["red", "yellow", "green"];
      this.lights = []; // в этом массиве хранятся все кнопочки эйкей фонарики
      this.activeIndex = 0;
      this.lighter;
      this.render();
      this.renderLights();
      this.startAutoSwitch();
    }
    toggleOffAll() {
      this.lights.forEach((light) => light.toggleOff());
    }
    renderLights() {
      this.colors.forEach((color) => {
        const light = new Light(this.lighter, color, () => {
          this.toggleOffAll()
          const currentIndex = this.lights.indexOf(light);
          this.activeIndex = currentIndex;
        });
        this.lights.push(light);
      });
  
    }
  
    stopAutoSwitch() {
      clearInterval(this.intervalId)
    }
    startAutoSwitch() {
      this.intervalId = setInterval(() => {
        this.automaticallyChangeColor();
      }, 1000)
    }
    render() {
      this.lighter = document.createElement("div");
      this.lighter.classList.add("traffic-lighter");
      this.lighter.onmouseenter = () => this.stopAutoSwitch();
      this.lighter.onmouseleave = () => this.startAutoSwitch();
      this.targetElement.appendChild(this.lighter);
    }
    toggleOn() {
      this.lights[0].toggleOn();
    }
    disable() {
      this.toggleOffAll();
    }
    validateNextIndex() {
      return this.activeIndex + 1 < this.colors.length
    }
    automaticallyChangeColor() {
        this.toggleOffAll();
        this.lights[this.activeIndex].toggleOn();
        this.activeIndex = this.validateNextIndex() ? this.activeIndex + 1 : 0;
    }
   }
  class Light {
    constructor(targetElement, color = "aqua", onClick) {
      this.color = color;
      this.targetElement = targetElement;
      this.onClick = onClick;
      this.isActive = false;
      this.render();
    }
  
    render() {
      this.element = document.createElement("button");
      this.element.classList.add("traffic-lighter__light");
      this.element.onclick = () => {
        if(this.onClick) this.onClick()
        this.toggle()
      }
      this.targetElement.appendChild(this.element);
    }
  
    toggle() {
      if (!this.isActive) {
        this.toggleOn();
      } else {
        this.toggleOff();
      }
    }
  
    toggleOn() {
      this.element.style.backgroundColor = this.color;
      this.element.classList.add("active");
      this.isActive = true;
    }
    toggleOff() {
      this.element.style.backgroundColor = "";
      this.element.classList.remove("active");
      this.isActive = false;
    }
  }
  class LighterController {
    constructor(targetElement) {
      this.targetElement = targetElement;
      this.lighters = [];
      this.render();
      this.renderButtons();
    }
  
    render() {
      const container = document.createElement("div");
      this.header = document.createElement("header");
      this.content = document.createElement("div");
  
      this.header.style.textAlign = "center";
      this.header.style.marginBottom = "30px";
      this.content.style.display = "flex";
      this.content.style.flexWrap = "wrap";
  
      container.appendChild(this.header);
      container.appendChild(this.content);
      this.targetElement.appendChild(container);
    }
  
    renderButtons() {
      const addButton = document.createElement("button");
      const toggleOnButton = document.createElement("button");
      const toggleOffButton = document.createElement("button");
      const removeButton = document.createElement("button");
  
      addButton.onclick = this.addLighter.bind(this);
      toggleOnButton.onclick = this.toggleOnLighters.bind(this);
      toggleOffButton.onclick = this.toggleOffLighters.bind(this);
      // removeButton.onclick = this.removeLighter.bind(this);
  
      addButton.innerHTML = "+";
      toggleOnButton.innerHTML = "On";
      toggleOffButton.innerHTML = "Off";
      removeButton.innerHTML = "&times";
  
      this.header.appendChild(addButton);
      this.header.appendChild(toggleOnButton);
      this.header.appendChild(toggleOffButton);
      this.header.appendChild(removeButton);
    }
  
    addLighter() {
      const lighter = new TrafficLighter(this.content);
      this.lighters.push(lighter);
    }
    // removeLighter() {
    //   if (this.lighters.length) {
    //     let lastLighter = this.lighters.pop();
    //   }
    // }
    toggleOnLighters() {
      this.lighters.forEach((lighter) => {
        // console.log(lighter.lights[0]);
        lighter.toggleOffAll();
        lighter.toggleOn();
      });
    }
    toggleOffLighters() {
      this.lighters.forEach((lighter) => lighter.toggleOffAll());
    }
  }
  //======== LIGHTER ========
  // const lighterController = new LighterController(document.querySelector(".OOP-Container"));
  
  /*const arr = [1,-3,6,9,2,4];
  let index = 0;
  
  const maxEl = arr.reduce((a, i, index) => {
      if(i > a[1]){
          return [i,index]
      } else {
          return a
      }
  },[arr,0])
  
  console.log(maxEl); */
  
  class Button {
    constructor(text, clickHandler) {
      this.text = text;
      this.clickHandler = clickHandler;
    }
    render = () => {
      this.target = document.querySelector(".OOP-Container");
      this.btn = document.createElement("button");
      this.btn.textContent = this.text || "button";
      this.btn.classList.add("btn", "btn-primary");
      this.btn.onclick = this.clickHandler;
      this.target.appendChild(this.btn);
    };
  }
  //======== LIGHTER ========++
  
  const handleModal = () => {
    const text = prompt();
    const newPar = document.createElement("p");
    const container = document.querySelector(".OOP-Container");
    newPar.textContent = text || "";
    container.appendChild(newPar);
  };
  
  // const newButton = new Button('Click to open modal', handleModal)
  // newButton.render()