document.addEventListener('click', (e) => {
    e.preventDefault();
    e.stopPropagation();
    if(e.target.hasAttribute('href')) {
        e.target.setAttribute('href', '')
    }
    e.target.remove();
})